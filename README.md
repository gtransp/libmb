<br />
<div align="left">
 
  <h2 align="left">libmb</h2>
  <p align="left">
    libmb is a joint library for heat balance or mass balance. This library creates the related heat balance terms, structs, functions which are further used for storing, printing the heat balance. The libmb library is now implemented into ProSe-PA software to manage the heat fluxes calculated by libseb library.
    <br />
    <br />
    ANSI C library developed at the Centre for geosciences and geoengineering, Mines Paris/ARMINES, PSL University, Fontainebleau, France.
    <br />
    <br />
    <strong>Contributors</strong>
    <br />
    Shuaitao WANG, Nicolas FLIPO
    <br />
    <br />
    <strong>Project Manager</strong>
    <br />
    Nicolas FLIPO
    <br />
    <br />
    <strong>Contact</strong>
    <br />
    Nicolas FLIPO <a href="mailto:nicolas.flipo@minesparis.psl.eu">nicolas.flipo@minesparis.psl.eu</a>
  </p>
</div>

## Copyright

[![License](https://img.shields.io/badge/License-EPL_2.0-blue.svg)](https://opensource.org/licenses/EPL-2.0)

&copy; 2023 Contributors to the libmb library.

*All rights reserved*. This software and the accompanying materials are made available under the terms of the Eclipse Public License (EPL) v2.0 
which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v20.html.