BEGIN{
  name="awk.out";
}
{
  if (NR==2)
    printf("#define VERSION_MB %4.2f\n",nversion)>name;
  else
    printf("%s\n",$0)>name;
}
