#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "GC.h"

#include "HYD.h"
#include "MB.h"

char *MB_name_process(int proc)
{
  char *name;

  switch(proc){
  case FIN_LATERAL_MB : {name = strdup("FIN_LATERAL");break;}
  case SW_MB : {name = strdup("SHORTWAVE");break;}
  case LW_MB : {name = strdup("LONGWAVE");break;}
  case LH_MB : {name = strdup("EVAPO_LATENT");break;}
  case SH_MB : {name = strdup("CONVECT_SENSIBLE");break;}
  case FIN_MB : {name = strdup("FIN");break;}
  case FOUT_MB : {name = strdup("FOUT");break;}
  case EINIT_MB : {name = strdup("EINIT");break;}
  case EEND_MB : {name = strdup("EEND");break;}
  case ERROR_MB : {name = strdup("ERROR");break;}
  default : {name = strdup("DEFAULT, heat mass balance process in itos.c");break;}
  }

  return name;
}
