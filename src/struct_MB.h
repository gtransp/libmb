typedef struct mass_balance_heat s_mbheat_mb;


struct mass_balance_heat {
  char *name;
  /*total number of mbs */
  int nmb;
  /* Times of beginning and end of the mass balance */
  double t[NEXTREMA_MB];
  /* Time step of the mass balance */
  double ndt;
  double t0;
  double time_unit;
  /* Tables defining whether or not temperature's mass balance si calculated */
  //int calc_mb_heat;
  double unit_mb_heat;
  
  /* Value of the mass balance on the current time step */
  double mbheat[NHEAT_TERMS_MB];
  
  s_lp_pk_hyd *pk_mb_heat;

  FILE *fpmb;
  /* Pointers towards next and previous mass balances */
  s_mbheat_mb *next;
  s_mbheat_mb *prev;

};

#define new_mb_heat_mb() ((s_mbheat_mb *) malloc(sizeof(s_mbheat_mb)))
