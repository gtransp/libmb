gawk -f update_param.awk -v nversion=$1 param_MB.h
mv awk.out param_MB.h

gawk -f update_doxygen.awk -v nversion=$1 test_doxygen
mv awk.out test_doxygen

gawk -f update_makefile.awk -v nversion=$1 Makefile
mv awk.out Makefile

echo "Version number updated in param.h, doxygen and Makefile cmd file to" $1
