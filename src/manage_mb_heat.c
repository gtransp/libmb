#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "GC.h"

#include "HYD.h"
#include "MB.h"


s_mbheat_mb *MB_init_mb_heat()
{
    s_mbheat_mb *pmb;
    
    pmb = new_mb_heat_mb();
    bzero((char *)pmb, sizeof(s_mbheat_mb));
    pmb->name = (char*)malloc(STRING_LENGTH_MB*sizeof(char));
    pmb->time_unit = 1.;
    pmb->unit_mb_heat = 1.;

    return pmb;
}


s_mbheat_mb *MB_chain_heat_mbs(s_mbheat_mb *pmb1, s_mbheat_mb *pmb2)
{
   pmb1->next = pmb2;
   pmb2->prev = pmb1;
  
   return pmb1;
}
